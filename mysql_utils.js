async function showTables(con) {
    const sqlQuery = "show tables;"
    const result = await con.query(sqlQuery)
    return result;
}

function getData(con) {
    return new Promise(function (resolve, reject){
        const sqlQuery = "select * from usertable"
        con.query(sqlQuery, function (err, resp) {
            if ( err) {
                reject("Something went wrong!")
            }
            resolve(resp)
        })

    })

}
function insertData(con, name, email, age) {
    return new Promise(function(resolve, reject) {
            const sqlQuery = `insert into usertable (name, email, age) values ('${name}', '${email}',${age})`;
            con.query(sqlQuery, function (err, resp) {
              if (err) {
                reject("Something went wrong")
              }
              resolve("Inserted");
            });

    })


    

}

function retriveDataApi(con, id) {
    return new Promise(function(resolve, reject) {
        const sqlQuery = `select * from usertable where id=${id}`;
        con.query(sqlQuery, function(err, resp) {
            if(err) {
                reject("Something went wrong")
            }
            resolve(resp)
        });
        

    })

}
function retriveData(con, email) {
    // `` -- backtick
    const sqlQuery = `select * from usertable where Email='${email}'`
    con.query(sqlQuery, function(err, resp) {
        if (err) {
            throw err
        }
        console.log(resp)
    })
}


module.exports = {
  showTables: showTables,
  getData: getData,
  insertData: insertData,
  retriveData: retriveData,
  retriveDataApi: retriveDataApi,
};