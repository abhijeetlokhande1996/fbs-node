const axios = require("axios");

async function getData() {
    const response = await axios.get("http://127.0.0.1:8000/api/emps/")
    return response.data   
}
async function postData() {

    const dataToInsert = {
      id: 25,
      name: "ABC",
    };
    const resp = await axios.post("http://127.0.0.1:8000/api/emps/", dataToInsert);
    return resp
  
}
postData().then(function(data) {
    getData().then(function(data) {
        console.log(data)
    })
})

