function afterFun() {
    console.log("afterFun");
}
function fun(){
    return new Promise(function(resolve, reject) {
            setTimeout(function () {
                // resolve("Resolve It")
                reject("Reject It");
            }, 1);
    })


}

fun().then(function (data) {
    console.log(data)
    afterFun()
}).catch(function(reason) {
    console.log(" In Catch")
})