const express = require("express")
const path = require("path")
const bodyParser = require("body-parser")
const session = require("express-session")
const cookieParser = require("cookie-parser")
const PORT = 3000
const app = express()
const viewDirPath = path.join(__dirname,"../views/templates")
const publicDirPath = path.join(__dirname, "../public")
app.use(express.static(publicDirPath));
app.use(bodyParser.urlencoded({extended:true}));
app.use(cookieParser());

app.use(session({
    secret:"root123"
}))

app.set("view engine","hbs")
app.set("views", viewDirPath)

app.get("/log-out", function(req, res) {
    req.session.destroy()
    res.redirect("/")
    res.end()
})
app.get("/", function(req, res){
    res.redirect("/index")
    res.end()
})
app.route("/index").get(function(req, res) {
    res.render("index")
}).post(function(req, res){
    const body = req.body
    const email = body.email;
    const password = body.password
    if ( email =="root@example.com" && password == "root123") {
        req.session.isAuthenticated = true
        res.redirect("/home")
        res.end()

    } else {
        res.render("index")
        res.end()
    }
    res.render("index")
})
app.get("/home", function(req, res){
    console.log(req.session);
    if ( !req.session || !req.session.isAuthenticated) {
        res.redirect("/err");
        res.end()
    }
    res.render("home")

})
app.get("/err", function(req, res) {
    
    res.render("err")
})
app.listen(PORT)