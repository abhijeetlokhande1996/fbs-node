const { showTables, getData, insertData, retriveData} = require("./mysql_utils")
const mysql = require("mysql")
const fs = require("fs")
const chalk = require("chalk")
const path = require("path")
const filePath = path.join(__dirname, "config","config.json")
let databaseCredential = fs.readFileSync(filePath, "utf-8")
databaseCredential = JSON.parse(databaseCredential)

// mysql connection
const con = mysql.createConnection(databaseCredential)
con.connect(function(err) {
    if (err) {
        throw err
    }
    console.log(chalk.green("Connected"))
    // getData(con).then(function(data){
    //     console.log(data)
    // }).catch(function(err) {
    //     console.log(err)
    // })
    retriveData(con,"mkpatil@gmail.com")
    con.end()
})