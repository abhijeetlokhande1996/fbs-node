const express = require("express")
const path = require("path")
const hbs = require("hbs")
hbs.registerHelper('toLowercase', function(str) {
    return str.toLowerCase();
})

const app = express()
const PORT = 3000
const publicDirPath = path.join(__dirname,"../public")
const viewsDirPath = path.join(__dirname,"../views/templates")
const partialDirPath = path.join(__dirname, "../views/partials")
app.use(express.static(publicDirPath))
hbs.registerPartials(partialDirPath)
app.set("view engine","hbs")
app.set("views", viewsDirPath)

app.get("/", function(req, res) {
    res.render("index", {
        "msg":"Welcome to Express App",
        "data": {
            "name":"Abhijeet Lokhande",
            "email":"abhijeetlokhande1996@gmail.com"
        },
        "names":["Abhijeet","Mahesh","Aasim"],
        "author":false
    })
})


app.listen(PORT, function(){
    console.log("Listening on ", PORT)
})