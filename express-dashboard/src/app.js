const chalk = require("chalk")
const express = require("express")
const path = require("path")
const app = express()
const hbs = require("hbs")
const PORT = 3000
const templatesDirPath = path.join(__dirname, "../views/templates")
const partialDirPath = path.join(__dirname, "../views/partials");
const publicDirPath = path.join(__dirname,"../public")

app.use(express.static(publicDirPath));
app.set("view engine","hbs")
app.set("views", templatesDirPath)

hbs.registerPartials(partialDirPath);

// helper function
hbs.registerHelper("upperCase", function(){
    //console.log(arguments[0])
    return arguments[0].toUpperCase();
})

app.get("/", function(req, res) {
    res.render("log_in", {
      name: "Abhijeet",
      email: "abhijeet@example.com",
      info:{
        "department":"Math",
        "grade":"A"
      },
      showHeading: false,
      nameArr:["Abhijeet","Mahesh","Aasim"]
    });
})
app.get("/about", function(req, res) {
  res.render('about')
})
app.get("/contact", function (req, res) {
  res.render("contact");
});
app.get("/inser_user", function (req, res) {
    res.render('insert_user')
})

app.listen(PORT, function() {
    console.log(chalk.green(`Server is started on ${PORT}`))
})
