const mysql = require("mysql")
const express = require("express")
const fs = require("fs")
const app = express()
const chalk = require("chalk")
const {
  showTables,
  getData,
  insertData,
  retriveData,
  retriveDataApi,
} = require("./../mysql_utils");
const PORT = 3000

const path = require("path");
const filePath = path.join(__dirname, "../config", "config.json");

let databaseCredential = fs.readFileSync(filePath, "utf-8");
databaseCredential = JSON.parse(databaseCredential);



app.use(function(req, res, next) {
    console.log("In Middleware")
    console.log(req.headers)
    if (req.headers['token'] != "ABCD123") {
        res.status(400)
        res.send({
            "err": "Unauthorised"
        })
        return

    }
    next()
})

app.use(express.json())


app.post("/auth", function(req, res) {
    const con = mysql.createConnection(databaseCredential);
    const email = req.body.email;
    const password = req.body.password
    const sqlQuery = `select * from auth where 
                        email='${email}'
                        and password = '${password}'
                        `
    con.query(sqlQuery, function(err, data) {
        if(err) {
            res.status(400)
            res.send({
                "err":"username and password are not matching"
            })
        }
        if(!data || data.length == 0) {
            res.status(400);
            res.send({
                err: "username and password are not matching",
            });

        }
        res.status(200)
        res.send({
            "token":"ABCD123"
        })
    });
})

app.get("/", function (req, res) {
    const objToSend = {
        "message":"Hello Express!"
    }
    res.send(objToSend);
})

app.get("/user", function(req,res) {
    

    const objToSend = {}
    const con = mysql.createConnection(databaseCredential);
    con.connect(function (err) {
        if (err) {
            throw err;
        }
        getData(con).then(function(data) {
            res.status(200)
            objToSend['result'] = 1;
            objToSend["data"] = data;

        }).catch(function(err) {
            res.status(500)
            objToSend["result"] = 0;
            objToSend["err"] = err;
        }).finally(function(){
            con.end()
            res.send(objToSend);
        }) 
    });



})

app.post("/user", function(req, res) {
    console.log("In User POSt")
    const name = req.body.name
    const age = req.body.age
    const email = req.body.email
    const objToSend = {};
    const con = mysql.createConnection(databaseCredential);
    con.connect(function (err) {
        if (err) {
            throw err;
        }
        insertData(con, name, email, age)
        .then(function(data){
            res.status(200)
            objToSend['result'] = 1
            objToSend['data'] = data

        }).catch(function(err) {
            res.status(500)
            objToSend["result"] = 0;
            objToSend["err"] = err;

        }).finally(function() {
            res.send(objToSend);

        })

    });

})

app.get("/user/:id", function(req, res){
    const id = req.params.id
    const con = mysql.createConnection(databaseCredential);
    const objToSend = {}
    con.connect(function(err) {
        if ( err) {
            throw err;
        }
        retriveDataApi(con, id).then(function(data) {
            res.status(200)
            objToSend['result'] = 1;
            objToSend["data"] = data;
        }).catch(function(err) {
            res.status(500);
            objToSend["result"] = 0;
            objToSend["err"] = err;
        }).finally(function(){
            res.send(objToSend);
        })
    })
  

})

app.listen(PORT, function(){
    console.log(chalk.green("Server started on PORT ", PORT))
})